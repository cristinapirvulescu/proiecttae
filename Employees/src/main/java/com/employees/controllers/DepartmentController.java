 
package com.employees.controllers;

import com.employees.entities.Department;
import com.employees.entities.dao.DepartmentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/departments")
public class DepartmentController {
     private DepartmentRepository repo;
    @Autowired
    public DepartmentController(DepartmentRepository repo) {
        this.repo = repo;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String getDepartments(Model model) {
        List<Department> departments =  repo.findAll();

         model.addAttribute("departments", departments);
        return "departments";
    }
   
    
}
