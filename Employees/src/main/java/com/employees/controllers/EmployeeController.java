package com.employees.controllers;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.employees.entities.Employee;
import com.employees.entities.dao.EmployeeRepository;

@Controller
public class EmployeeController {
    private EmployeeRepository repo;
  
    @Autowired
    public EmployeeController(EmployeeRepository repo) {
        this.repo = repo;
    }
    
    @GetMapping("/")
    public String getEmployees(Model model) {
        model.addAttribute("employees", repo.findAll());
        model.addAttribute("employee", new Employee());
        model.addAttribute("jobs", Employee.getJobs());
        model.addAttribute("departments", Employee.getDepartments());
        return "employees";
    }
        
    @PostMapping("/")
    @Transactional
    public String addEmployee(@ModelAttribute Employee employee) {
        repo.save(employee);
        return "redirect:/";
    }
    
     @GetMapping("/{id}")
    public String selectEmployee(@PathVariable("id") Long id, HttpSession session) {
        session.setAttribute("current", repo.findOne(id));
        
        return "redirect:/";
    }
    
    @DeleteMapping("/{id}")
    @Transactional
    public String deleteEmployee(@PathVariable("id") Long id) {
        repo.delete(id);
        return "redirect:/";
    }
    
    @PostMapping("/{id}/save")
    @Transactional
    public String saveEmployee(@PathVariable("id") Long id, @ModelAttribute Employee employee, 
            HttpSession session) {
       
        Employee e = repo.findOne(id);
        e.setFirstName(employee.getFirstName());
        e.setLastName(employee.getLastName());
        e.setSalary(employee.getSalary());
        e.setJob(employee.getJob());
        e.setDepartmentName(employee.getDepartmentName());
    
        session.invalidate();
        return "redirect:/";
    }
}
