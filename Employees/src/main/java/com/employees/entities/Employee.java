package com.employees.entities;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@Entity
@NoArgsConstructor @AllArgsConstructor
public class Employee implements Serializable{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Long id;
    
    private String firstName;
    private String lastName;
    private Double salary;
    private String job;
    private String departmentName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName=departmentName;
    }
    
    public static List<String> getJobs(){
        String [] jobs = {"Accountant","Accounting Manager", "Business Analyst",
            "Programmer", "Purchasing manager", "Sales Representative"};
        
        return Arrays.asList(jobs);
    }
    
    public static List<String> getDepartments(){
        String [] departments = {"Accounting","IT", "Marketing",
            "Purchasing", "Sales"};
        
        return Arrays.asList(departments);
    }
        
       
    
}
