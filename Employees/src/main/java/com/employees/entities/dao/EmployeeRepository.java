/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.employees.entities.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.employees.entities.Employee;
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    
}
